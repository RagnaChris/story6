from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import chromedriver_binary

# Create your tests here.
class Story6Test(TestCase):
	def test_uji_is_exist(self):
		response = Client().get('/uji/')
		self.assertEqual(response.status_code, 200)

	def test_uji_using_uji_template(self):
		response = Client().get('/uji/')
		self.assertTemplateUsed(response, 'uji.html')

	def test_uji_using_uji_func(self):
		found = resolve('/uji/')
		self.assertEqual(found.func, uji)

	def test_models_can_create_status(self):
		s = Status.objects.create(msg="Coba Coba", date=timezone.now())
		counting = Status.objects.count()
		self.assertEqual(counting, 1)

	def test_can_save_a_POST_request(self):
		response = Client().post('/uji/', data={'msg': 'Coba Coba', 'date':'2019-10-28T16:00'})
		counting = Status.objects.count()
		self.assertEqual(counting, 1)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/uji/')
		more = Client().get('/uji/')
		html_response = more.content.decode('utf8')
		self.assertIn('Coba Coba', html_response)
